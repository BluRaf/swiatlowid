# -*- coding: utf-8 -*-
from irc3.plugins.command import command
import irc3
from struct import *
from string import Template
import socket, sys, time, datetime
from urllib.parse import urlsplit

@irc3.plugin
class Mumble:

    def __init__(self, bot):
        self.bot = bot

    def mumble_ping(self, host, port):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.settimeout(1)

        buf = pack(">iQ", 0, datetime.datetime.now().microsecond)
        try:
            s.sendto(buf, (host, port))
        except socket.gaierror:
            return "Nie można się połączyć z serwerem (nieznany host)"

        try:
            data, addr = s.recvfrom(1024)
        except socket.timeout:
            return "Nie można połączyć się z serwerem (upłynął czas odpowiedzi)"

        r = unpack(">bbbbQiii", data)

        version = '.'.join([str(v) for v in r[1:4]])
        ts = r[4]
        users = r[5]
        max_users = r[6]
        bandwidth = f"{r[7] / 1000}kbit/s"

        ping = (datetime.datetime.now().microsecond - r[4]) / 1000.0
        if ping < 0:
            ping = ping + 1000
        ping = f"{ping:.1f}ms"

        lut = {
          'h': host,
          'P': port,
          'v': version,
          't': ts,
          'u': users,
          'm': max_users,
          'p': ping,
          'b': bandwidth,
        }
        t = Template("mumble://$h:$P/ -> $u/$m słuchaczy, ping $p, pasmo $b, wersja $v")
        return t.substitute(**lut)


    @command(permission='view')
    def mumble(self, mask, target, args):
        """Status serwera Mumble. Wspierane są URL-e "mumble://" oraz niestandardowe porty.

            %%mumble <url>
        """
        url = args["<url>"]
        if "//" not in url:
            url = "//" + url

        netloc = urlsplit(url, scheme='mumble').netloc.split(":")
        host = netloc[0]
        try:
            port = int(netloc[1])
        except (IndexError, ValueError):
            port = 64738

        yield self.mumble_ping(host, port)
