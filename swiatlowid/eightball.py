# -*- coding: utf-8 -*-
from irc3.plugins.command import command
import irc3
from struct import *
from string import Template
import socket, sys, time, datetime
from urllib.parse import urlsplit
import random

@irc3.plugin
class Eightball:

    EN_RESPONSES = [
        "It is certain", "It is decidedly so", "Without a doubt", "Yes definitely", "You may rely on it",
        "As I see it, yes", "Most likely", "Outlook good", "Yes", "Signs point to yes",
        "Reply hazy, try again", "Ask again later", "Better not tell you now", "Cannot predict now", "Concentrate and ask again",
        "Don't count on it", "My reply is no", "My sources say no", "Outlook not so good", "Very doubtful"
    ]

    RESPONSES = [
        "To pewne", "Zdecydowanie tak", "Bez wątpienia", "Zdecydowanie tak", "Możesz na tym polegać",
        "Moim zdaniem, tak", "Najprawdopodobniej", "Dobre perspektywy", "Tak", "Znaki wskazują na tak",
        "Odpowiedź mglista, spróbuj ponownie", "Zapytaj ponownie później", "Lepiej ci tego nie mówić teraz", "Nie mogę teraz przewidzieć", "Skoncentruj się i zapytaj ponownie",
        "Nie licz na to", "Moja odpowiedź brzmi nie", "Moje źródła mówią nie", "Perspektywy nie są dobre", "Bardzo wątpliwe"
    ]

    POPE_RESPONSES = [
        "A jak pan Jezus powiedział?", "Tak jak pan Jezus powiedział", "Jeszcze jak", "Co mam robić?", "Dość.", "Jest możliwe", "Jak mi dadzą, to jem", "Tak sobie", "Też ujdą"
    ]

    def __init__(self, bot):
        self.bot = bot

    @command(permission='view')
    def osemka(self, mask, target, args):
        """Spolszczony Magic 8 Ball

            %%osemka <pytanie>...
        """

        yield random.choice(self.RESPONSES)

    @command(permission='view')
    def popeball(self, mask, target, args):
        """Papieski Magic 8 Ball

            %%popeball <pytanie>...
        """

        yield random.choice(self.POPE_RESPONSES)
